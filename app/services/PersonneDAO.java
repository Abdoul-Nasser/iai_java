package services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author ABOUL-NASSER
 *
 */
public class PersonneDAO extends JDBC {

	public String ajouter(Personne c) throws ClassNotFoundException, SQLException {

		try {
			prepareStat = this.jdbc().prepareStatement("insert into  iai.personne values (default, ?, ?, ?) ");
			prepareStat.setString(1, c.getName());
			prepareStat.setString(2, c.getPrenom());
			prepareStat.setInt(3, c.getAge());
			// prepareStat.setDate(4, new java.sql.Date(2009, 12, 11));
			prepareStat.executeUpdate();
			return "success";
		} catch (SQLException e) {
			return e.getMessage();
		}

	}

	public List<Personne> listes() throws ClassNotFoundException, SQLException {
		statment = this.jdbc().createStatement();
		List<Personne> listes = new ArrayList<Personne>();
		
		Personne pers = new Personne();
		// les resultat de la requete sont stocker dans l'objet result
		result = statment.executeQuery("select * from iai.personne");

		while (result.next()) {
			pers.setId(Integer.valueOf(result.getString("id")));
			pers.setName(result.getString("nom"));
			pers.setPrenom(result.getString("prenom"));
			pers.setAge(Integer.valueOf(result.getString("age")));
			listes.add(pers);
			// on viende l'objet personne avant de repasser la boucle
			pers = new Personne();
		}
		//retourne la listes constitué
		return listes;

	}

	public Personne findByID(int id) throws ClassNotFoundException, SQLException {

		statment = this.jdbc().createStatement();
		Personne c = new Personne();
		// les resultat de la requete sont stocker dans l'objet result
		prepareStat = this.jdbc().prepareStatement("select * from iai.personne where id =? ");
		prepareStat.setInt(1, id);
		
		result = prepareStat.executeQuery();

		if (result.next()) {

			c.setId(Integer.valueOf(result.getString("id").replace(" ", "")));
			c.setName(result.getString("nom"));
			c.setPrenom(result.getString("prenom"));
			c.setAge(Integer.valueOf(result.getString("age")));

		}

		return c;

	}

	public String suppression(int id) throws SQLException, ClassNotFoundException {
	
		prepareStat = this.jdbc().prepareStatement("DELETE FROM iai.personne WHERE id=?");
		prepareStat.setInt(1, id);
		 
		int rowsDeleted = prepareStat.executeUpdate();
		if (rowsDeleted > 0) {
		    System.out.println("Personne deleted successfully!");
		    return "OK";
		}else
			return "erreur";
	}

	public String update(Personne p) throws SQLException {
		try {
			String sql = "UPDATE iai.personne SET nom=?,prenom=?,age=? WHERE id=?";
			 
			prepareStat = this.jdbc().prepareStatement(sql);
			prepareStat.setString(1, p.getName());
			prepareStat.setString(2, p.getPrenom());
			prepareStat.setInt(3, p.getAge());
			prepareStat.setInt(4, p.getId());
			
			int rowsUpdated = prepareStat.executeUpdate();
			
			if (rowsUpdated > 0) {
			    System.out.println("Personne was updated successfully!");
			}
			return "ok";
		} catch (Exception e) {
			return e.getMessage();
		}

	}

}
