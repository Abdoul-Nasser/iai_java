package controllers;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Http.Request;
import services.Personne;
import services.PersonneDAO;
import play.mvc.Result;

public class PersonnesCtrl extends Controller {

	FormFactory dataForm;
	PersonneDAO personneDAO;

	@Inject
	public PersonnesCtrl(FormFactory dataForm, PersonneDAO personneDAO) {
		super();
		this.dataForm = dataForm;
		this.personneDAO = personneDAO;
	}

	public Result ajoutPersonne(Request request) throws ClassNotFoundException, SQLException {

		//List<Personne> maListe = personneDAO.listes();
		
		Form<Personne> form = dataForm.form(Personne.class).bindFromRequest(request);
		Personne personne = form.get();
		
		if(personneDAO.ajouter(personne).equals("success"))
			System.out.println("ajouter effectuer avec success");
		else
			System.err.println("modification non effectuer" + personneDAO.ajouter(personne));
		
		
		//personneDAO.listes();
		
		return ok(views.html.test.render(personneDAO.listes()));

	}

	

	public Result afficherFormulaire(Request request) throws ClassNotFoundException, SQLException{
		return ok(views.html.personne.render(request));
	}
	
	public Result modifView(Request request, int id) throws ClassNotFoundException, SQLException{
		return ok(views.html.update.render(personneDAO.findByID(id),request));
	}
	
	
	public Result update(Request request) throws ClassNotFoundException, SQLException{
		Form<Personne> form = dataForm.form(Personne.class).bindFromRequest(request);
		Personne personne = form.get();
		
		if(personneDAO.update(personne).equals("ok"))
			System.out.println("Modification effectuer avec success");
		else
			System.err.println("Modification non effectuer" + personneDAO.update(personne));
		
		
		return ok(views.html.test.render(personneDAO.listes()));

	}
	
	public Result deletePersonne(Request request, int id) throws ClassNotFoundException, SQLException{
		personneDAO.suppression(id);
		
		return ok(views.html.test.render(personneDAO.listes()));

	}

}
